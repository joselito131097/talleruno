package model.domain;

import utils.ClaseDESBase64;

public class Datos {
	ClaseDESBase64 des=new ClaseDESBase64();
	
	public int idusu;
	public String login;
	public String password;
	public Boolean activo;
	
	public int getIdusu() {
		return idusu;
	}


	public void setIdusu(int idusu) {
		this.idusu = idusu;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getActivo() {
		return activo;
	}


	public void setActivo(Boolean activo) {
		this.activo = activo;
	}


	public String getCifrarPassword()
	{
		String xpassword=String.valueOf(this.password);
		String res=des.encriptar(xpassword);
		return res;
	}
}
