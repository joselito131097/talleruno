
package model.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import model.domain.Datos;
import model.domain.Usuarios;

//@Service indica que la clase es un bean de la capa de negocio
@Service
public class DatosManager {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	public Datos validarLogin(String xlogin){
		BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(Datos.class);
		String xsql="select *  "
		+"  from datos "
		+" where login=?";
		Datos aux=null;
		try {
			aux= (Datos) this.jdbcTemplate.queryForObject(xsql,new Object[] {xlogin},bprm);
		}
		catch(Exception e)
		{
			aux=null;
		}
		/*catch(EmptyResultDataAccessException exception)
		{
			aux.login="";
		}
		catch(NullPointerException exception)
		{
			aux.login="";
		}*/
		return aux;
	}
	public List<Map<String,Object>> listarMenus(int xidrol){
		String xsql="select m.idmenu,m.nombre,m.activo "
		+"  from menus m, rolme r "
		+"  where r.idmenu=m.idmenu and r.idrol=? ";		
					
		return this.jdbcTemplate.queryForList(xsql, new Object[] {xidrol});
	}	
	public List<Map<String,Object>> listarSubmenu(){
		String xsql="select m.idmenu,p.nombre,p.enlace,p.descripcion "
		+"  from procesos p, mepro m "
		+"  where m.idpro=p.idpro";		
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarRoles(){
		String xsql="select m.idrol , m.nombre "
		+"   from roles  as m ";		
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarTabla(){
		String xsql="select * " 
	              +"  from usuarios ";		
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarRolesUsu(int IDUSU){
		String xsql="select b.IDROL,c.nombre " + 
				"from USUROL b,roles c " + 
				"where  b.IDUSU="+IDUSU+" and c.IDROL=b.IDROL;";		
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public Usuarios extraerDatos(int xidusu){
		BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(Usuarios.class);
		String xsql="select *"
		+"  from usuarios "
		+" where idusu=?";
		Usuarios aux=null;
		try {
			aux= (Usuarios) this.jdbcTemplate.queryForObject(xsql,new Object[] {xidusu},bprm);
		}
		catch(Exception e)
		{
			
			aux=null;
		}
		/*catch(EmptyResultDataAccessException exception)
		{
			aux.login="";
		}
		catch(NullPointerException exception)
		{
			aux.login="";
		}*/
		return aux;
	}
}