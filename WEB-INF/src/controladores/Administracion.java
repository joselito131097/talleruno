
package controladores;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import model.domain.Datos;
import model.domain.Usuarios;
import model.manager.DatosManager;
import utils.ClaseDESBase64;


		@Controller
		public class Administracion {
			
			@Autowired
			DatosManager datosManager=new DatosManager();
			
			@RequestMapping({"index.html"})
			public String vista1(Model model)  throws IOException  {
				
				return "inicio";
			}
			@RequestMapping({"mensaje.html"})
			public String vista2(Model model)  throws IOException  {
				model.addAttribute("xtexto", "El usuario no existe");
				return "mensajes";
			}
			@RequestMapping({"valida.html"})
			public String valida(Model model,HttpServletRequest req,HttpServletRequest request, HttpServletResponse response)  throws IOException, ParseException  {
			 try {
				 String zlogi=req.getParameter("xlog");
					String zpassword=req.getParameter("xpass");
					Datos alu = this.datosManager.validarLogin(zlogi.toLowerCase());
					 int xidusu=alu.idusu;
					Usuarios usu = this.datosManager.extraerDatos(xidusu);
					if(alu==null)
					{
						model.addAttribute("xtexto", "El usuario no existe" );
						return "mensajes";
					}
					else
					{
						if(alu.password.equals(zpassword))
						{
							HttpSession sesion= request.getSession(true);
							sesion.setAttribute("xlogi",zlogi);
							model.addAttribute("xnombre", usu.nombre );
							model.addAttribute("xap", usu.apellido1 );
							model.addAttribute("xam", usu.apellido2 );
							
							List<?> listasroles = this.datosManager.listarRoles();
							model.addAttribute("xlistasrole", listasroles );
							List<?> listasrolesusu = this.datosManager.listarRolesUsu(usu.idusu);
							model.addAttribute("xlistasrole", listasrolesusu );
										
						
							return "menu";
						}
						else
						{
							model.addAttribute("xtexto", "El password es incorrecto" );
							return "mensajes";
						}
					}
			} catch (Exception e) {
				System.err.println("Exception caught: " + e.getMessage());
			}
			 model.addAttribute("xtexto", "El usuario es incorrecto" );
				return "mensajes";
			}
			@RequestMapping({"listamenus.html"})
			public String listamenus(Model model,HttpServletRequest req)  throws IOException  {
				
				String xidrol=req.getParameter("xrol");
				
				//System.out.println("llego tambien:"+xidrol);
				
				List<?> listamenu = this.datosManager.listarMenus(Integer.parseInt(xidrol));
				model.addAttribute("xlistamenu", listamenu );	

				List<?> listasubmenu = this.datosManager.listarSubmenu();
				model.addAttribute("xlistasubmenu", listasubmenu );			
						
				
				return "lismenu";
			}	
				
			@RequestMapping({"menu.html"})
			public String vista3(Model model,HttpServletRequest req,HttpServletRequest request, HttpServletResponse response)  throws IOException  {
				HttpSession sesion= request.getSession(true);
				String zlogi=(String) sesion.getAttribute("xlog");
				if (zlogi==null){
					model.addAttribute("xtexto", "ingrese primero" );
					return "mensajes";
				}else{
					
					return "menu";
				}
			}
			@RequestMapping({"desconectar.html"})
			public String desconectar(Model model,HttpServletRequest req,HttpServletRequest request, HttpServletResponse response)  throws IOException  {
				HttpSession sesion=request.getSession(true);
				sesion.removeAttribute("xlog");		
				model.addAttribute("xtexto", "Desconectado" );
				return "inicio";
			}
			@RequestMapping({"listaTabla.html"})
			public String listaTabla(Model model,HttpServletRequest req)  throws IOException  {
		
				List<?> listartable = this.datosManager.listarTabla();
				model.addAttribute("xlistarTable", listartable );	
				System.out.println(listartable);				
				return "table";
			}	
			
		}



